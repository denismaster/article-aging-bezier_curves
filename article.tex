% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{microtype}
\usepackage{array,tabularx}

\newenvironment{conditions*}
  {\par\vspace{\abovedisplayskip}\noindent
   \tabularx{\columnwidth}{>{$}l<{$} @{${}={}$} >{\raggedright\arraybackslash}X}}
  {\endtabularx\par\vspace{\belowdisplayskip}}

\usepackage{listings} % Оформление исходного кода
\lstset{
    basicstyle=\scriptsize\ttfamily, % Размер и тип шрифта
    breaklines=true,            % Перенос строк
    tabsize=2,                  % Размер табуляции
    frame=single,               % Рамка
    literate={--}{{-{}-}}2,     % Корректно отображать двойной дефис
    literate={---}{{-{}-{}-}}3  % Корректно отображать тройной дефис
}

% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}
%
\title{Visualization and spline approximation of the process of functioning of a complex system}
%
\titlerunning{Visualization and approximation of CS functioning process}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Doronina Julia\inst{1} \and
Obolensky Denis\inst{1} }
%
\authorrunning{J. Doronina, D. Obolensky.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Sevastopol State University, Sevastopol 299053, Russia \\ \email{info@sevsu.ru} }
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
A model of the process of changing the efficiency of an integrated system during its life cycle is considered.

The expediency of applying various distribution laws in analyzing the degradation process of complex systems is determined.

An example of visualization and spline approximation of the functioning process for the purpose of further analysis is given.

\keywords{Visualization  \and Complex system \and Spline \and Approximation \and Degradation.}
\end{abstract}
%
%
%
\section{Introduction}

The importance of studying the process of degradation of complex systems (CS) is due to the following factors: the rapid improvement of technology, up to the emergence of fundamentally new; the increasing complexity of requirements for systems and the increasing complexity of the CS itself.
In addition, based on the complex nature of the CS, the developer needs to have decision-making mechanisms for its development with various components.
Consider complex systems such as information systems (IS). 
According to Federal Law No. 149-ФЗ~\cite{FederalLaw}, an information system is a combination of information contained in databases and information technologies and technical means ensuring its processing.
According to ISO/IEC 2382-1: 1993~\cite{ISO} IS is an information processing system and corresponding organizational resources (human, technical, financial, etc.) that provide and distribute information.
Thus, the heterogeneity of the objects included in the IS, their weak formalizability lead to the fact that the effectiveness of the IS can be unpredictable and sharply decrease in the process of functioning.
In this regard, the presence of a formal description of the obsolescence process plays an important role in predicting the response of an IS to a given requirement.

The existing methods in the field of analysis and improvement of IS explores the concept of a life cycle, but do not directly take into account changes in efficiency over time due to system degradation. 
Models of system degradation are mainly based on the statement of the exponential law of degradation, or the law is not mentioned at all~\cite{Barinov,Dagabyan}.
In order to build an adequate model of a real system, one should take into account the rate of change of the system's efficiency during its life cycle, the process of system degradation and the readiness of the system for reengineering \cite{Doronina}.

Thus, it is advisable to determine the CS degradation models based on various laws of distribution of random variables, as well as to obtain calculations of the system efficiency parameters when it is outdated, based on an analysis of the dependence of these parameters on the type of distribution law.

The purpose of this work is to build models of CS degradation and establish its links with the type of distribution law and justify the expediency of using more sophisticated methods for approximating the function of CS lifecycle based on splines, and also to consider possible visualizations of these models.

\section{Compex System's efficiency and degradation}
In the general case, a change in the system performance over time $E(t)$ can be represented as:
\begin{equation} \label{eq:oldModel}
    E(t) = \begin{cases}
        E_0,            & \; t<t_m         \\
        E_0 \cdot f(t), & \; t<t_m         \\
        E^r_0,          & \; t>t_r \\
    \end{cases} \
\end{equation}
where:
\begin{conditions*}
        E(t)       &   system performance at a given time \\
        E_0       &   initial system efficiency \\
        t_m       &   start time of moral and / or technical degradation of the system \\
        t_r       &   time of the beginning of intensive improvement (reengineering) of the system \\
        f(t)       &   system degradation function
\end{conditions*}

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./images/fig1.png}
    \caption{Scheme of the system degradation model}
    \label{fig:model}
\end{figure}

The figure above (see Fig.~\ref{fig:model}) is a diagram of the degradation of the complex system.
On the first segment $E(t) = const$.
On the segment $t \geq t_m$, the efficiency index falls and is determined in the general case by some function $f(t)$.
The third segment characterizes the reengineering period $E ^ r_0$, which increases the efficiency indicator relative to the initial system efficiency $E_0$.

The theoretical range of reengineering flexibility is illustrated as the area of ​​the $ABC$ triangle.
The upper part of the triangle can be deformed and limited, for example, by the exponent function and determine the actual range of flexibility.
At the same time, the exponential bending can be different, which has a significant impact on the indicator of the effectiveness of the system.
The time to make a decision about the beginning of reengineering is related to the output of the system's efficiency to the permissible limit (specified in the documentation) and is determined by the next condition:

\begin{equation} \label{eq:condition}
    E_0(t) \leq E_{min}
\end{equation}
where:
\begin{conditions*}
        E_{min}  &  minimum acceptable level of system efficiency
\end{conditions*}

The software implementation of this model in TypeScript is presented in the listing below:

\begin{lstlisting}
    export type EffectivnessFunction = (...criteria: number[]) => number;
    export type Effectivness = number | EffectivnessFunction;
    export interface System {
        initialEffectivnes: Effectivness;
        agingStartMoment: number;
        reengineeringCoefficient: number;
        agingEndMoment?: number;
        agingFunction: AgingFunction;
        minimalEffectivness?: number;
    }
    export type AgingFunction = ({ system: System, t: number }) => number;
    export default function (system: System, t: number, ...rest): number {
        let initialEffectivness = 0;
        if (typeof (system.initialEffectivnes) === "function")
            initialEffectivness = 
            system.initialEffectivnes(...rest);
        else
            initialEffectivness = 
                system.initialEffectivnes;
        if (t <= system.agingStartMoment) {
            return initialEffectivness;
        }
        if (system.agingEndMoment) {
            return initialEffectivness * 
                system.reengineeringCoefficient;
        }
        const effectivness = system.agingFunction({ system, t });
        if(effectivness < system.minimalEffectivness) 
            return initialEffectivness * system.reengineeringCoefficient;
        return effectivness;
    }
\end{lstlisting}

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./images/base2.png}
    \caption{Logarithmic function $y=\ln{x}$}
    \label{fig:goodRLn}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./images/base4.png}
    \caption{Degradation function $y=x^2$.}
    \label{fig:high_available}
\end{figure}

We study the process of system's aging on the segment $t_m \leq t \leq t_r$.
Consider the logarithmic and power dependencies, selecting for each case the corresponding parameters $E_0$,
$k$ in such a way as to achieve maximum proximity of the approximating and table function.

The coefficients of approximating functions of different types were calculated starting from the point $x_0 = 2$.
The values ​​of $x$ are uniformly distributed with a step of $D = 0.5$ and the values ​​of the function $y = {0.8 0.65 0.53 0.43 0.35 0.29 0.24}$,
taken under the conditions: $k = 0.4$ and $T_m = 2 months $ (ee Fig.~\ref{fig:goodRLn}).

Table~\ref{tabl_1} shows the coefficients of the corresponding regression type, which most approximate the approximating and tabular functions.

\begin{table}
\caption{Type of elementary function and its coefficients}\label{tabl_1}
\begin{tabular}{|l|l|}
\hline
Type of function & Coefficients \\
\hline
$y = ae ^ {bx} + c $ & $ a = 1.8; b = -0.4; c = 0.033 $ \\
\hline
$y = a \ ln (x + b) + c $ & $ a = -0.45; b = -1.6; c = 0.85 $ \\
\hline
$ y = ax ^ {1/2} + c $ & $ a = 2.4; c = -1; $ \\
\hline
\end{tabular}
\end{table}

To clarify the degradation function, it is proposed to classify situations in the process of system degradation as follows:

A - consider the system to be weakly obsolete, if after the onset of moral obsolescence (in a period equal to 1 working cycle of the system)
the change in its efficiency does not exceed 5-10%.
As in Fig.~\ref{fig:model}, this situation is reflected by the function $E_0 \cdot e^{k(t-t_m)}$.

B - to consider the system to be rapidly becoming obsolete, if after the onset of degradation (in the period equal to 1 working cycle of the system)
change in its efficiency index exceeds 20%.

C - to consider the system as an obsolete(high-availability system). These cases are shown in Fig.~\ref{fig:classification}.

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./images/classification.png}
    \caption{Degradation types classification}
    \label{fig:classification}
\end{figure}

During the research it was revealed that in the case of B - high readiness, it is advisable to use laws such as $y = x ^ 2$, $y = x ^ 3$.
The scheme of realization of such a model for the functions $y = x ^ 2$ is shown in Fig.~\ref{fig:high_available}
under the conditions: $k = 0.4$ and $T_m = 2 $ month.
Software implementation of this approach in TypeScript is presented in the listing below:

\begin{lstlisting}
export const linearModel: AgingFunction = ({ system: System, t: number }, k:number) => {
    t_m = system.agingStartMoment;
    return system.initialEffectivnes * (-k) * (t-t_m);
}

export const quadraticModel: AgingFunction = ({ system: System, t: number }, k:number) => {
    t_m = system.agingStartMoment; 
    return system.initialEffectivnes * (-k) * Math.pow((t-t_m),2);
}
\end{lstlisting}

\section{Spline approximation and visualization}

To clarify the type of the system degradation function, we can use the spline approximation.
This method not only simplifies the appearance of the function, but also reduces the loss of accuracy when performing calculations on a computer.
In this paper we will check how to use Splines and Bezier Curves to approximate and visualize degradation function.

\subsection{Using splines}
Mathematically speaking, several elements are required to define a spline: a degree, a set of contiguous intervals, and, of course, polynomials on each of this intervals.
The degree of a spline, denoted $d$ in this section, is the maximal degree of the polynomial pieces.
Equivalently, one may talk about the order of the spline, which is $d+1$ for a spline of degree $d$.
The intervals are defined using a knot sequence $k_0 < \ldots < k_n$ which is a strictly increasing sequence of $n+1$ real numbers.
The values $k_0 , \ldots , k_n$ are called the \emph{knots}.
These knots defines $n$ \emph{knot intervals}  $[k_i,k_{i+1}]$ for $i \in [ 0,n-1 ] $.
On each knot interval, a spline of degree $d$ is defined by a polynomial of degree at most $d$.
The \emph{natural definition domain} of a spline is the interval $[k_0,k_n]$ \cite{Splines}.
For instance, a spline $s:[k_0,k_n] \rightarrow \mathbb{R}$ may be defined as:
\begin{equation}
s(x) =
\sum_{i=0}^d p_{ij} (x - k_j)^i \qquad
\begin{cases}
	\textrm{if } x \in [k_j,k_{j+1}[ \textrm{ and } j \in [ 0,n-2 ] \\
	\textrm{if } x \in [k_{n-1},k_n]
\end{cases}
\end{equation}
where:
\begin{conditions*}
    p_{ij}  &  the coefficients of the $j$-th polynomial piece ($j \in [ 0,n-1 ] $)
\end{conditions*}

The splines expressed as a linear combination of B-splines. 
The B-splines are a set of piecewise polynomial functions that defines a suitable basis for the vector space of the splines\cite{Splines}.

The B-spline $ N_{i,d+1}$ of degree $ d$ (order $ d+1$) with knots $ k_i < \ldots < k_{i+d+1}$ is defined recursively with the following relations (\ref{eq:splinesdef1}, \ref{eq:splinesdef2}):

\begin{equation} \label{eq:splinesdef1}
N_{i,d+1}(x) = \frac{x - k_i}{k_{i+d} - k_i} N_{i,d}(x) + \frac{k_{i+d+1} - x}{k_{i+d+1} - k_{i+1}} N_{i+1,d}(x),
\end{equation}
\begin{equation} \label{eq:splinesdef2}
N_{i,1}(x)= 
\begin{cases} 
1 & \textrm{if } x \in [k_i,k_{i+1}] \\ 
0 & \textrm{otherwise.} 
\end{cases}
\end{equation}

We can use splines to interpolate and approximate degradation function. 
Consider a situtation when we have dataset with complex system's effiiciency data\cite{Splines2}.
In that case, we can use splines to approximate data using different approaches(see Fig.~\ref{fig:splines}) \cite{Splines}:
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{./images/approx.png}
    \caption{Cubic spline interpolation and least errors interpolation}
    \label{fig:splines}
\end{figure}

Consider we have efficiency data $(x_i,y_i)_{i=1}^{m}$. We need to find a spline $g$ with the next property\cite{Splines2}:
\begin{equation}
    g(x_i)=\sum_{j=1}^{m}c_j B_{j,d}(x_i)=y_i
\end{equation}
where:
\begin{conditions*}
    g  & spline we need to find
\end{conditions*}

There are several options to find basis functions and coefficients \cite{Splines2}.
At (\ref{eq:splines}, \ref{eq:splines2}) we can see equations in matrix form that we can use to calculate that approximation:

\begin{equation} \label{eq:splines}
Ac = 
\begin{pmatrix}B_{1,d}(x_1)&\ldots &B_{m,d}(x_1)\\\vdots&\ddots&\vdots\\B_{1,d}(x_m)&\ldots&B_{m,d}(x_m)\end{pmatrix} 
\begin{pmatrix} c_1 \\ \vdots \\ c_m \end{pmatrix}
=
\begin{pmatrix} y_1 \\ \vdots \\ y_m \end{pmatrix} 
=y
\end{equation}
\begin{equation} \label{eq:splines2}
A^{T}Ac^{*} = A^{T}b
\end{equation}

When we have the spline $g$, we can solve the equation (\ref{eq:splinemodel}) to find the best moment to begin reengineering:
\begin{equation} \label{eq:splinemodel}
    g(x)=M
\end{equation}
where:
\begin{conditions*}
    g  & spline \\
    M & minimal acceptable efficiency
\end{conditions*}

The best approach to use spline method is when we need we have some data about the complex system's efficiency and we need to find the efficiency value at random time or find a moment at time when we need to start reengineering.

\subsection{Using Bezier curves}
Bezier curves are special cases of Bershtein polynomials and are generally determined by the expression \ref{eq:bezier}.

\begin{equation}\label{eq:bezier}
    B(n, t) = \sum \limits_ {i = 0} ^ {N} \begin{pmatrix} n \\ i \end{pmatrix} \cdot (1-t) ^ {ni} \cdot t ^ i,  t \in [0,1]
\end{equation}
where:
\begin{conditions*}
    B(n, t)  &  a Bezier curve \\
    t & the parameter value \\
    N & the order of the curve
\end{conditions*}

For $N = 2$, the curve is defined by 3 control points (quadratic curve), and for $N = 3$ - by 4 control points (cubic curve) \cite{Pomax}.
An example of the order 3 curve is shown in Fig.~\ref{fig:bezierTypes}.
The most common curves has second or third orders. Their equation is given in the formulas \ref{eq:quadratic} and \ref{eq:cubic} \cite{Pomax}.

\begin{equation}\label{eq:quadratic}
    b_{2, t} = (1-t) ^ 2 + 2 \cdot (1-t) \cdot t + t ^ 2, t \in [0,1]
\end{equation}
\begin{equation} \label{eq:cubic}
    b_{3, t} = (1-t) ^ 3 + 3 \cdot (1-t) ^ 2 \cdot t + 3 \cdot (1-t) \cdot t ^ 2 + t ^ 3, t \in [0,1],
\end{equation}
where:
\begin{conditions*}
    t & the parameter value 
\end{conditions*}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{./images/fig2.png}
    \caption{One of the possible types of quadratic Bezier Curves}
    \label{fig:bezierTypes}
\end{figure}

The most efficient curve-drawing algorithm is the de-Castelgio \cite{Pomax} algorithm.

Consider the use of curves in the problem of analyzing the effectiveness of the system.
Let the initial value of system's efficiency $E_0$ be given and the first control point has coordinates $(0, E)$, and the second one - $(1,0)$.
We can go to the interval $(0,1)$ instead of the explicit indication of the time when the efficiency value decreases conditionally to $0$.

Let the first point denote the moment of time $t_m$, and the second point - the moment when the efficiency of the system due to degradation has decreased
to the minimum value.
Then you can draw a straight line and get a linear degradation function.
Given one or two control points, we get a Bezier curve that approximates this process.

There is an inverse de Castelgio algorithm for determining the reference points of a curve with given parameters \cite{Pomax}.
An example of the algorithm is presented in Fig.~\ref{fig:reverseCasteljo} and Fig.~\ref{fig:reverseCasteljo2}.

A sample code that implements the de Castelgio algorithm is presented below:

\begin{lstlisting}
function getPoints(result, p, t) {
    let x, y;
    if (p.length === 1) {
        result = [...result, ...p];
    }
    else {
        let inner = new Array(p.length);
        for (let i = 0; i < inner.length; i++) {
            x = (1 - t) * p[i].x + t * p[i + 1].x;
            y = (1 - t) * p[i].y + t * p[i + 1].y;
            inner[i] = { x, y };
        }
        getPoints(result, inner, t)
    }
}
function deCasteljau(controlPoints, ratio) {
    let points = [];
    t = 0;
    while (t <= 1) {
        getPoints(points, controlPoints, t);
        t += ratio;
    }
} 
\end{lstlisting}

By specifying the control points manually or applying the inverse algorithm, you can calculate the function that describes the given curve \cite{Pomax}.

\begin{figure}
    \centering
    \includegraphics[width=0.25\linewidth]{./images/fig3.png}
    \caption{An example of the work of the inverse de Casteljo algorithm}
    \label{fig:reverseCasteljo}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.25\linewidth] {./images/fig4.png}
    \caption{An example of the work of the inverse algorithm de Casteljo}
    \label{fig:reverseCasteljo2}
\end{figure}

To take into account the minimum allowable efficiency of the system, we consider the function $M(t)=M$, where $M$ is the minimum efficiency value that is admissible within a given subject area.

Let a curve $B(t)$ be also given.
The x-part of the intersection point of the curve $B(t)$ and the straight line $M(t)$ will be the desired time when it is necessary to implement the reengineering of the system.
We obtain the system of equations \ref{eq:model}. The system for a cubic curve is constructed in a similar way.

\begin{equation}\label{eq:model}
    B(t) = \begin{cases}
        y = M, \\
        t = 0 \cdot (1-t) ^ 2 + 2 \cdot (1-t) \cdot t \cdot w_ {1x} + t ^ 2, \\
        y = E \cdot (1-t) ^ 2 + 2 \cdot (1-t) \cdot t \cdot w_ {1y}, \\
        t \in [0,1]
    \end{cases} 
\end{equation}

Solving the system of equations \ref{eq:model}, you can get the value of $t$ - the desired time of the beginning of reengineering.

The best way to use Bezier curves is when we need to define \emph{how} our complex system's efficiency will change during the time.

\subsection{Examples of using the method}
When researching real systems, the shape of the degradation process may differ from conventional models.
At the same time, during the development of an integrated system, it is rather difficult to predict the features of its further development due to the unevenness of financial investments and the expenditure of forces on development in the support phase \cite{Litvak}.

\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{./images/exp_vs_curve.png}
    \caption{Comparizon between spline and exponential function}
    \label{fig:exp_vs_curve}
\end{figure}

Consider the situation presented in Fig.~\ref{fig:exp_vs_curve}, which shows the change in efficiency
municipal system, in the example of a system for calculating charges for housing and communal services.
The $X$ axis shows the week numbers of the system, and the $Y$ axis shows the system efficiency.

This system is intensively degrading.
This type of process is due to various factors: new tariffs and calculation methods are being introduced, resources are being spent on their development; there is a tendency for the growth of citizens turning to the cents; hardware and software generates a specific resource.
At the same time, production and human resources at the phase of supporting the housing and utilities information system are rather limited, which affects the decrease in its efficiency. On the other hand, in the support phase, additional resources may be attracted, including specialists in the field of information technology, which in general will affect the change in the type of degradation function.
An example of such a process can be represented by not one but several bends of the degradation curve.
The presence of inflection points is the result of an attempt to influence the life cycle of the system and increase the efficiency of the system during its operation.
This is possible with the involvement of additional financial and human resources.

The use of ordinary functions, in particular, the exponential function $E (t) = e ^ {- k (t-t_m)}$, will only show the nature of the process.
The coefficient $k$, which determines the slope of the curve, determines its slope from the very beginning of the process.
The use of the exponential funtion is shown in Fig.~\ref{fig:exp_vs_curve}.

Approximation of such a function using ordinary nonlinear functions will lead to the use of piecewise defined functions,
which complicates the solution of the system of equations.

The use of splines or higher-order Bezier curves ($N \geq 4$) makes it possible to better describe such complex degradation processes.
It also makes it possible to describe not only the nature of the process, but also attempts to influence it.
The use of splines is also show in Fig.~\ref{fig:exp_vs_curve}.

The approximation error determined using the method of least squares is shown in Table ~\ref{tabl_3}, 
where you can see a significant advantage of approximation of a complex system's degradation degradation function by splines.

\begin{table}[h!]
\caption{Approximation errors for different approaches}\vspace*{2mm}
\centering 
\small
\label{tabl_3}
\begin{tabular}{|c|c|}
    \hline
    Approach             & Error          \\
    \hline
    Piecewise function & $\epsilon=0.56$ \\
    \hline
    Spline & $\epsilon=0.07$ \\
    \hline
\end{tabular}
\end{table}

Let see how this method can be applied to analyze the process of degradation of technical systems \cite{Degradation}.
Consider the analysis of the propagation of cracks in turbines for power plants.

One type of turbine blades has been tested for crack propagation.
The test blocks test the blades every 100,000 cycles.
A complete failure is defined as a crack with a length of 30mm or more.

An example of increasing the crack length is given in the table \ref{tabl_4}

\begin{table}[h!]
    \caption{length of crack in blade}\vspace*{2mm}
    \centering\small\label{tabl_4}

    \begin{tabular}{|c|c|}
        \hline
        Number of cycles (x1000) & Crack length (mm) \\
        \hline
        100 & 15 \\
        \hline
        200 & 20 \\
        \hline
        300 & 22 \\
        \hline
        400 & 26 \\
        \hline
        500 & 29 \\
        \hline
    \end{tabular}
\end{table}

Using the Bezier curve, we obtain the following model for analysis(see Table.~\ref{tabl_5}):
\begin{table}[h!]
    \caption{Control points of a Bezier curve}\vspace*{2mm}
    \centering\small\label{tabl_5}

    \begin{tabular}{|c|c|}
        \hline
        $ x $ & $ y $ \\
        \hline
        500 & 29 \\
        \hline
        374.4 & 22.6 \\
        \hline
        217.6 & 21.2 \\
        \hline
        100 & 15 \\
        \hline
    \end{tabular}
\end{table}

With them we can calculate the number of cycles when the crack exceeds 30mm. The required number of cycles will be $500.74$.

Based on this method, the development of a decision support system (DSS) has begun to control the process of functioning of both the entire IS and its individual components.
The use of DSS allows the analysis of the lifecycle of complex system, the approximation of the process of obsolescence and forecasting.
This approach also helps decision makers to determine the optimal time for the start of the process of system reengineering.

% \section{First Section}
% \subsection{A Subsection Sample}
% Please note that the first paragraph of a section or subsection is
% not indented. The first paragraph that follows a table, figure,
% equation etc. does not need an indent, either.

% Subsequent paragraphs, however, are indented.

% \subsubsection{Sample Heading (Third Level)} Only two levels of
% headings should be numbered. Lower level headings remain unnumbered;
% they are formatted as run-in headings.

% \paragraph{Sample Heading (Fourth Level)}
% The contribution should contain no more than four levels of
% headings. Table~\ref{tab1} gives a summary of all heading levels.

% \begin{table}
% \caption{Table captions should be placed above the
% tables.}\label{tab1}
% \begin{tabular}{|l|l|l|}
% \hline
% Heading level &  Example & Font size and style\\
% \hline
% Title (centered) &  {\Large\bfseries Lecture Notes} & 14 point, bold\\
% 1st-level heading &  {\large\bfseries 1 Introduction} & 12 point, bold\\
% 2nd-level heading & {\bfseries 2.1 Printing Area} & 10 point, bold\\
% 3rd-level heading & {\bfseries Run-in Heading in Bold.} Text follows & 10 point, bold\\
% 4th-level heading & {\itshape Lowest Level Heading.} Text follows & 10 point, italic\\
% \hline
% \end{tabular}
% \end{table}


% \noindent Displayed equations are centered and set on a separate
% line.
% \begin{equation}
% x + y = z
% \end{equation}
% Please try to avoid rasterized images for line-art diagrams and
% schemas. Whenever possible, use vector graphics instead (see
% Fig.~\ref{fig1}).

% \begin{figure}
% \includegraphics[width=\textwidth]{./images/fig1.png}
% \caption{A figure caption is always placed below the illustration.
% Please note that short captions are centered, while long ones are
% justified by the macro package automatically.} \label{fig1}
% \end{figure}

% \begin{theorem}
% This is a sample theorem. The run-in heading is set in bold, while
% the following text appears in italics. Definitions, lemmas,
% propositions, and corollaries are styled the same way.
% \end{theorem}
% %
% % the environments 'definition', 'lemma', 'proposition', 'corollary',
% % 'remark', and 'example' are defined in the LLNCS documentclass as well.
% %
% \begin{proof}
% Proofs, examples, and remarks have the initial word in italics,
% while the following text appears in normal font.
% \end{proof}
% For citations of references, we prefer the use of square brackets
% and consecutive numbers. Citations using labels or the author/year
% convention are also acceptable. The following bibliography provides
% a sample reference list with entries for journal articles.

\section{Conclusion}
The study of the approximation of the function, reflecting the degradation process of information systems, showed its possible variability in various cases of the functioning of the system. 
The logarithmic and power-law functions were chosen as the approximating functions. 
During the analysis of the obtained models, it was found that the type of distribution law significantly affects the model of the degradation of the analyzed system.
For the case of a sharply change in efficiency, for example, when introducing new technologies that significantly affect its performance, it is advisable to use the following law: $y = \sqrt{x}$.
Depending on the required rate of change of the degradation process, it is possible to use elementary functions $y = e^x$ or $y = \ln{x}$. 
If the system efficiency decreases slowly, then it is desirable to use the functions $y = x^2$ or $y = x^3$.

Based on the results of analytical modeling, a classification of cases of degradation of complex systems has been proposed. 
It is shown that the approach based on the use of splines can also be applied to approximate complex cases of degradation.
We can use splines and Bezier curves to find complex system's efficiency at the given point at time or to find the point when efficiency get required value.
It also makes it possible to plan the degradation process with the required characteristics.

The development of DSS based on the proposed method has begun. 
This DSS will allow solving the tasks related to obtaining optimal terms for intensifying the investment of funds to slow down the degradation process or the timing of the start of the reengineering of complex systems.

%~\cite{ref_article1}, an LNCS chapter~\cite{ref_lncs1}, a
%book~\cite{ref_book1}, proceedings without editors~\cite{ref_proc1},
%and a homepage~\cite{ref_url1}. Multiple citations are grouped
%\cite{ref_article1,ref_lncs1,ref_book1},
%\cite{ref_article1,ref_book1,ref_proc1,ref_url1}.
%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
% \bibliographystyle{splncs04}
% \bibliography{mybibliography}
%
    \begin{thebibliography}{8}
        \bibitem{FederalLaw} 
        Federal Law of 27.07.2006 N 149-FZ "On Information, Information Technologies and on the Protection of Information",
        \url{http://www.consultant.ru/document/cons\_doc\_LAW\_173622} . 
        Last accessed 12 Apr 2017

        \bibitem{ISO} 
        ISO/IEC 2382-1:1993 Information technology; Vocabulary; Part 1: Fundamental terms,
        \url{http://rossert.narod.ru/alldoc/info/2z23/g28647.html}. 
        Last accessed 20 Dec 2018

        \bibitem{Barinov}
        Barinov, V. Reengineering: the essence and methodology, \url{http://www.ippnou.ru/article.php?idarticle=002369}. 
        Last accessed  19 May 2017
        
        \bibitem{Dagabyan}
        Dagabyan, A. Design of technical systems. Mechanical engineering, Moscow (1986)

        \bibitem{Doronina}
        Doronina, J. -V. Management model for the improvement of an automated information system based on flexible reengineering. In:
        Vesnik SevSTU "Automation of processes and management", pp. 107-110.  SevSTU, Sevastopol (2012)
        
        \bibitem{Litvak}
        Litvak, B. -V. Development of management decisions: 5th edn. Delo, Moscow (2004)

        \bibitem{Splines}
        Paramethric Models of Function, \url{www.brnt.eu/phd/node11} . 
        Last accessed 10 Mar 2019

        \bibitem{Splines2}
        Spline Approximation of Functionsand Data, \url{https://folk.uio.no/in329/nchap5.pdf}. 
        Last accessed 9 Mar 2019

        \bibitem{Pomax} 
        A Primer on Bézier Curves, \url{https://pomax.github.io/bezierinfo}. 
        Last accessed 5 Nov 2018

        \bibitem{Degradation} 
        Degradation Analysis Wiki, \url{http://www.reliawiki.org/index.php/Degradation\_Data\_Analysis}. 
        Last accessed 11 Nov 2017

    \end{thebibliography}

\end{document}
